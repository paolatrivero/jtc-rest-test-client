package py.com.jtc.rest.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import py.com.jtc.rest.client.bean.Movimiento;
import py.com.jtc.rest.client.bean.Usuario;
import py.com.jtc.rest.client.service.CuentaService;
import py.com.jtc.rest.client.service.MovimientoService;
import py.com.jtc.rest.client.service.UsuarioService;

@Controller
@RequestMapping("/rest-movimiento")
public class MovimientoController {
	
	@Autowired
	private MovimientoService movimientoService;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private CuentaService cuentaService;

	@GetMapping("/movimiento/{idcuenta}")
	public String listaMovimiento(@PathVariable("idcuenta") Integer idcuenta, Model model) { 
		model.addAttribute("cuenta", cuentaService.obtenerUnaCuenta(idcuenta));
		model.addAttribute("usuario", usuarioService.obtenerUsuarioCta(idcuenta));
		model.addAttribute("movimiento", movimientoService.obtenerMovCuenta(idcuenta));
		return "movimientos";
	}
	
	@GetMapping("/movimiento")
	public String listamovimiento(Model model) { 
		model.addAttribute("movimiento", movimientoService.obtenerMovimiento());
		return "movimientos";
	}
	
	@GetMapping
	public String index() {
		return "movimientos";
	}
	
	@GetMapping("/movimiento/add")
	public String movimientoForm(Model model) {
		model.addAttribute("movimiento", new Movimiento());
		return "form";
	}
}
