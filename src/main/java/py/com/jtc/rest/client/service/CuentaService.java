package py.com.jtc.rest.client.service;

import java.util.List;
import py.com.jtc.rest.client.bean.Cuenta;
import py.com.jtc.rest.client.bean.Transferencia;

public interface CuentaService {
	
	List<Cuenta> obtenerCuenta();
	Cuenta obtenerUnaCuenta(Integer idcuenta);
	List<Cuenta> obtenerCtaUsuario(Integer idusuario);
	//List<Cuenta> obtenerUnaCta(Integer idcuenta);
	void insertarCuenta(Cuenta comentario);
	Cuenta editarCuenta(Integer idcuenta, Cuenta c);
	void eliminarCuenta(Integer idcuenta);
	void ctaTransferencia(Transferencia transferencia);

}
