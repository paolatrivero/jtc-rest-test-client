package py.com.jtc.rest.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import py.com.jtc.rest.client.bean.Cuenta;
import py.com.jtc.rest.client.bean.Transferencia;
import py.com.jtc.rest.client.service.CuentaService;

@Controller
@RequestMapping("/rest-cuenta")
public class CuentaController {

	@Autowired
	private CuentaService cuentaService;

	@GetMapping("/cuenta/{idusuario}")
	public String listaCuenta(@PathVariable("idusuario") Integer idusuario, Model model) { 
		model.addAttribute("cuenta", cuentaService.obtenerCtaUsuario(idusuario));
		return "cuentas";
	}
	
	@GetMapping("/cuenta/transferencia/{idcuenta}")
	public String TransferenciaCuenta(@PathVariable("idcuenta") Integer idcuenta, Model model) { 
		Cuenta cuenta = cuentaService.obtenerUnaCuenta(idcuenta);
		
		Transferencia t = new Transferencia();
		t.setCtadebito(cuenta.getNrocuenta());
		t.setIdcuenta(cuenta.getIdcuenta());
		t.setMoneda(cuenta.getMoneda());
		t.setSaldo(cuenta.getSaldo());
				
		model.addAttribute("transferencias", t);
		return "transferencia";
	}

	/*@GetMapping("/cuenta/prueba/{idcuenta}")
	public String UnaCta(@PathVariable("idcuenta") Integer idcuenta, Model model) { 
		model.addAttribute("cuentas", cuentaService.obtenerUnaCta(idcuenta));
		return "transferencia";
	}*/
	
	@GetMapping("/cuenta")
	public String listaCuenta(Model model) { 
		model.addAttribute("cuenta", cuentaService.obtenerCuenta());
		return "cuentas";
	}
	@GetMapping
	public String index() {
		return "cuentas";
	}
	
	@PostMapping("/ctatransferencia")
	public String ctaTransferencia(@ModelAttribute("transferencias") Transferencia transferencia) {
		cuentaService.ctaTransferencia(transferencia);
		Transferencia t = new Transferencia();
		t.setCtadebito(transferencia.getCtadebito());
		t.setCtacredito(transferencia.getCtacredito());
		t.setMonto(transferencia.getMonto());
		t.setMoneda(transferencia.getMoneda());
		
		return "fintransferencia";
		//return "redirect:/rest-cuenta/cuenta";
	}
}
