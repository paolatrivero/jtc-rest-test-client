package py.com.jtc.rest.client.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import py.com.jtc.rest.client.bean.Movimiento;

@Service
public class MovimientoServiceImpl implements MovimientoService {
	private static List<Movimiento> movimiento;
	public static final String URL_API = "http://localhost:8090/rest/movimiento/";
	
	@Override
	public List<Movimiento> obtenerMovimiento() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Movimiento[]> responseEntity = restTemplate.getForEntity(URL_API, Movimiento[].class);
		movimiento = Arrays.asList(responseEntity.getBody());
		return movimiento;
	}
	
	@Override
	public List<Movimiento> obtenerMovCuenta(Integer idcuenta) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("idcuenta",idcuenta);
	    
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Movimiento[]> responseEntity = restTemplate.getForEntity(URL_API+"{idcuenta}", Movimiento[].class, params);                                                  
		movimiento = Arrays.asList(responseEntity.getBody());

		return movimiento;
	}
	
	@Override
	public void insertarMovimiento(Movimiento movimiento) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Movimiento> responseEntity = restTemplate.postForEntity(URL_API, movimiento, Movimiento.class);
	}


}
