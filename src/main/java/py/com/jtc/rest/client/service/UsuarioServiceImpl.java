package py.com.jtc.rest.client.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import py.com.jtc.rest.client.bean.Usuario;
import py.com.jtc.rest.client.bean.UsuarioLogin;

@Service
public class UsuarioServiceImpl implements UsuarioService {
	private static List<Usuario> usuario;
	public static final String URL_API = "http://localhost:8090/rest/usuario/";
	
	@Override
	public List<Usuario> obtenerUsuario() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Usuario[]> responseEntity = restTemplate.getForEntity(URL_API, Usuario[].class);
		usuario = Arrays.asList(responseEntity.getBody());
		return usuario;
	}
	
	@Override 
	public Usuario obtenerUsuario(Integer idusuario) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Usuario> responseEntity = restTemplate.getForEntity(URL_API+"{idusuario}", Usuario.class);

		Usuario usuario = responseEntity.getBody();
		return usuario;
	}

	@Override 
	public Usuario obtenerUsuarioCta(Integer idcuenta) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("idcuenta",idcuenta);
	    
		System.out.println("serUsuario 1....");
		RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Usuario> responseEntity = restTemplate.getForEntity(URL_API+"{idcuenta}", Usuario.class, params);
		Usuario usuario = responseEntity.getBody();
		return usuario;
	}
	
	@Override
	public List<Usuario> obtenerUsuLogin(Integer idusuario) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("idusuario",idusuario);
	    
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Usuario[]> responseEntity = restTemplate.getForEntity(URL_API+"{idusuario}", Usuario[].class, params);
		usuario = Arrays.asList(responseEntity.getBody());

		return usuario;
	}
	
	@Override
	public boolean usuLogin(String nrodocumento) {
		RestTemplate restTemplate = new RestTemplate();
		UsuarioLogin user = new UsuarioLogin();
		user.setNrodocumento(nrodocumento);
		
		UriComponentsBuilder urlBuilder =  UriComponentsBuilder.fromHttpUrl(URL_API);
		urlBuilder.path("login");
		Boolean response = restTemplate.postForObject(urlBuilder.toUriString(), user, Boolean.class);
		return response;
	}

}
