package py.com.jtc.rest.client.service;

import java.util.List;

import py.com.jtc.rest.client.bean.Usuario;

public interface UsuarioService {
	List<Usuario> obtenerUsuario();
	List<Usuario> obtenerUsuLogin(Integer idusuario);
	Usuario obtenerUsuario(Integer idusuario);
	Usuario obtenerUsuarioCta(Integer idcuenta);
	boolean usuLogin(String nrodocumento);
}
