package py.com.jtc.rest.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import py.com.jtc.rest.client.service.UsuarioService;

@Controller
@RequestMapping("/rest-usuario")
public class UsuarioController {
	@Autowired
	private UsuarioService usuarioService;

	@GetMapping("/usuario/{idusuario}")
	public String listaMovimiento(@PathVariable("idusuario") Integer idusuario, Model model) { 

		model.addAttribute("usuario", usuarioService.obtenerUsuLogin(idusuario));
		return "usuario";
	}
	
	@GetMapping("/login")
	public String login(Model model) { 

		model.addAttribute("usuario", usuarioService.obtenerUsuario());
		return "login";
	}
	@PostMapping("/login/{nrodocumento}")
	public String usuLogin(@PathVariable("nrodocumento") String nrodocumento, Model model) { 

		model.addAttribute("usuario", usuarioService.usuLogin(nrodocumento));
		return "login";
	}
	
	@GetMapping("/usuario")
	public String listaCuenta(Model model) { 

		model.addAttribute("usuario", usuarioService.obtenerUsuario());
		return "usuario";
	}
	@GetMapping
	public String index() {
		return "usuario";
	}

}
