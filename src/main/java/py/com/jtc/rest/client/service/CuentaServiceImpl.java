package py.com.jtc.rest.client.service;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import py.com.jtc.rest.client.bean.Cuenta;
import py.com.jtc.rest.client.bean.Transferencia;

@Service
public class CuentaServiceImpl implements CuentaService{
	private static List<Cuenta> cuenta;
	public static final String URL_API = "http://localhost:8090/rest/cuenta/";
	
	@Override
	public List<Cuenta> obtenerCuenta() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta[]> responseEntity = restTemplate.getForEntity(URL_API, Cuenta[].class);
		cuenta = Arrays.asList(responseEntity.getBody());
		return cuenta;
	}
	
	@Override
	public Cuenta obtenerUnaCuenta(Integer idcuenta) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("idcuenta",idcuenta);
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta> responseEntity = restTemplate.getForEntity(URL_API+"transferencia/{idcuenta}", Cuenta.class, params);
		
		Cuenta cuenta = responseEntity.getBody();
		return cuenta;
	}

	@Override
	public List<Cuenta> obtenerCtaUsuario(Integer idusuario) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("idusuario",idusuario);
	    
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta[]> responseEntity = restTemplate.getForEntity(URL_API+"{idusuario}", Cuenta[].class, params);                                                  
		cuenta = Arrays.asList(responseEntity.getBody());

		return cuenta;
	}

	/*@Override
	public List<Cuenta> obtenerUnaCta(Integer idcuenta) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("idcuenta",idcuenta);
	    
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta[]> responseEntity = restTemplate.getForEntity(URL_API+"/transferencia/{idcuenta}", Cuenta[].class, params);                                                  
		cuenta = Arrays.asList(responseEntity.getBody());

		return cuenta;
	}*/

	@Override
	public void insertarCuenta(Cuenta cuenta) {
		//TODO: pendiente
	}
	
	@Override
	public Cuenta editarCuenta(Integer idcuenta, Cuenta cuenta) {
		//TODO: Actualizar lo necesario en esta funcion
		return new Cuenta();
	}
	
	@Override
	public void eliminarCuenta(Integer idcuenta) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("idcuenta",idcuenta);
	    RestTemplate restTemplate = new RestTemplate();
	    restTemplate.delete(URL_API+"{idcuenta}",params);
	}
	@Override
	public void ctaTransferencia(Transferencia transferencia) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.postForEntity(URL_API, transferencia, Transferencia.class);
		
	}
}
