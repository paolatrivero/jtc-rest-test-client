package py.com.jtc.rest.client.service;

import java.util.List;

import py.com.jtc.rest.client.bean.Movimiento;

public interface MovimientoService {
	List<Movimiento> obtenerMovimiento();
	List<Movimiento> obtenerMovCuenta(Integer idcuenta);
	void insertarMovimiento(Movimiento comentario);
}
